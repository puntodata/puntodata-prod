/*$("body").easeScroll({
  frameRate: 60,
  animationTime: 250,
  stepSize: 40,
  pulseAlgorithm: !0,
  pulseScale: 8,
  pulseNormalize: 1,
  accelerationDelta: 10,
  accelerationMax: 1,
  keyboardSupport: !0,
  arrowScroll: 25
});*/


//Up top
$(function() {
    $(".smooth").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 80
            }, 500, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
    /*paceOptions = {
     initialRate:0.7,
       minTime:10750,
       maxProgressPerFrame:1,
       ghostTime: 10000
    }*/
    window.paceOptions = {
        startOnPageLoad: false
    }
    Pace.on('start', function(e) {

        //$('body').addClass('init--splashScreen')
    })

    Pace.start()

    Pace.on('done', function() {

    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > 400) {
            var windowHeight = $(window).scrollTop();
            var contenido2 = $(".whatdo").offset();
            contenido2 = contenido2.top;

            if (windowHeight >= contenido2) {
                $('.primary--nav').addClass('fixed--nav')
            } else {
                $('.primary--nav').removeClass('fixed--nav')
            }


        }
    })

    // Show or hide the sticky footer button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.go-top').addClass('expand');
            $('.page-header-alpha').addClass('scroll')
        } else {
            $('.go-top').removeClass('expand');
            $('.page-header-alpha').removeClass('scroll')
        }
    });

    // Animate the scroll to top
    $('.go-top').click(function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    })


});
// Magic line
function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        fn();

    } else {
        document.addEventListener('DOMContentLoaded', fn);

    }
};

function setupNav() {
    var wee = document.querySelector('.wee').style,
        navList = document.querySelectorAll('.link--item'),
        currentNav = document.querySelector('.current-menu-item');

    navList.forEach(function(nav) {
        nav.addEventListener('click', function() {
            wee['left'] = this.offsetLeft - currentNav.offsetLeft + 'px';
            wee['width'] = this.offsetWidth + 'px';
        });

        /*nav.addEventListener('mouseout', function() {
          wee['left'] = '0';
          wee['width'] = currentNav.offsetWidth + 'px';
        });*/
    });
};

ready(function() {
    //setupNav();
    var wee = document.querySelector('.wee').style,
        navList = document.querySelectorAll('.link--item'),
        currentNav = document.querySelector('.current-menu-item'),
        navActive = document.querySelector('.link--item.active');

    wee['left'] = 0 + 'px';
    wee['width'] = navList.offsetWidth + 'px';




});

// Click

const links_b = document.getElementById('menu--items');
const link_b = links_b.getElementsByTagName('li');



for (var i = 0; i < link_b.length; i++) {
    link_b[i].addEventListener('click', function(event) {
        event.preventDefault();

        var current_b = document.getElementsByClassName('current-menu-item');
        current_b[0].className = current_b[0].className.replace(' current-menu-item', '');
        this.className += " current-menu-item";


    })
}



$(document).ready(function() {


}); //end ready





/* Please ❤ this if you like it! */



(function($) {
    "use strict";

    //Switch dark/light

    var h = $('.slider-wrapper').height();
    //$('.rounder-ball-1').css('height', h + 'px')
    //$('.rounder-ball-1').css('width', h + 'px')
    //$('.rounder-ball-1').css('width', h + 'px')





    $(".switch").on('click', function() {
        if ($("body").hasClass("light")) {
            $("body").removeClass("light");
            $(".switch").removeClass("switched");
        } else {
            $("body").addClass("light");
            $(".switch").addClass("switched");
        }
    });

    $(document).ready(function() {
        "use strict";

        //Scroll back to top

        var progressPath = document.querySelector('.progress-wrap path');
        var pathLength = progressPath.getTotalLength();
        progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
        progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
        progressPath.style.strokeDashoffset = pathLength;
        progressPath.getBoundingClientRect();
        progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
        var updateProgress = function() {
            var scroll = $(window).scrollTop();
            var height = $(document).height() - $(window).height();
            var progress = pathLength - (scroll * pathLength / height);
            progressPath.style.strokeDashoffset = progress;
        }
        updateProgress();
        $(window).scroll(updateProgress);
        var offset = 50;
        var duration = 550;
        jQuery(window).on('scroll', function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('.progress-wrap').addClass('active-progress');
            } else {
                jQuery('.progress-wrap').removeClass('active-progress');
            }
        });
        jQuery('.progress-wrap').on('click', function(event) {
            event.preventDefault();
            jQuery('html, body').animate({
                scrollTop: 0
            }, duration);
            return false;
        })


    });

})(jQuery);


(function() {
    $('.ham1').click(function() {
        $('.menuReveal').toggleClass('open')
    })

})();
/*(function() {
  var navEl = document.querySelector('nav.menu'),
    revealer = new RevealFx(navEl),
    closeCtrl = navEl.querySelector('.btn--close');

  document.querySelector('.btn--menu').addEventListener('click', function() {

    revealer.reveal({
      bgcolor: '#7f40f1',
      duration: 400,
      easing: 'easeInOutCubic',
      onCover: function(contentEl, revealerEl) {
        navEl.classList.add('menu--open');
        contentEl.style.opacity = 1;
      },
      onComplete: function() {
        closeCtrl.addEventListener('click', closeMenu);
      }
    });
  });

  function closeMenu() {
    closeCtrl.removeEventListener('click', closeMenu);
    navEl.classList.remove('menu--open');
    revealer.reveal({
      bgcolor: '#7f40f1',
      duration: 400,
      easing: 'easeInOutCubic',
      onCover: function(contentEl, revealerEl) {
        navEl.classList.remove('menu--open');
        contentEl.style.opacity = 0;
      },
      onComplete: function() {

      }
    });
  }
})();*/

$('.grid__item').click(function() {
    $('.details__addtocart').attr('href', $(this).find('.product__url').text())

})



$(document).ready(function() {
    //var
    var $nav = $('.nav--items'),
        $line = $('<div>').appendTo($nav),
        $activeLi,
        lineWidth,
        liPos;

    function refresh() {
        $activeLi = $nav.find('li.active');
        lineWidth = $activeLi.outerWidth();
        liPos = $activeLi.position().left;
    }
    refresh();

    $nav.css('position', 'relative');

    //line setup
    function lineSet() {
        $line.css({
            'position': 'absolute',
            'background-color': '#e6a31b',
            'bottom': '-12px',
            'height': '4px',
            //'width': '10px',
            'border-radius': '6px'
        }).animate({
            'left': liPos,
            'width': lineWidth
        }, 300, function() {
            //alert(0)
        }).promise().done(function() {
            //alert(1)
        })
    }
    lineSet();

    //on click
    $nav.find('li').on('click', function() {

        $activeLi.removeClass('active');
        $(this).addClass('active');
        refresh();
        lineSet();
    });
    $(document).on("scroll", onScroll);

    //smoothscroll
    $('.nav--items a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.nav--items li a').each(function() {
            $(this).parent().removeClass('active');
        })
        $(this).parent().addClass('active');

        var target = this.hash,
            menu = target;

        var $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 80
        }, 500, 'swing', function() {
            window.location.hash = target;
            $(document).on("scroll", onScroll);

        });
    });


});

function onScroll(event) {

    var scrollPos = $(document).scrollTop();
    $('.nav--items li a').each(function() {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (currLink.position().top <= scrollPos &&
            currLink.position().top + currLink.height() > scrollPos) {
            $('.nav--items li a').parent().removeClass("active");
            currLink.parent().addClass("active");
        } else {
            currLink.parent().removeClass("active");
        }
    });
}


var overlaps = (function() {
    function getPositions(elem) {
        var pos, width, height;
        pos = $(elem).position();
        width = $(elem).width();
        height = $(elem).height();
        return [
            [pos.left, pos.left + width],
            [pos.top, pos.top + height]
        ];
    }

    function comparePositions(p1, p2) {
        var r1, r2;
        r1 = p1[0] < p2[0] ? p1 : p2;
        r2 = p1[0] < p2[0] ? p2 : p1;
        return r1[1] > r2[0] || r1[0] === r2[0];
    }

    return function(a, b) {
        var pos1 = getPositions(a),
            pos2 = getPositions(b);
        return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
    };
})();

$(function() {
    var area = $('#area')[0],
        box = $('#box0'),
        html;

    html = $(area).children().not(box).map(function(i) {
        if (overlaps(box, this)) {
            $('#box2').addClass('overlaps')
            console.log(box)
        }

        console.log(box)
        return '<p>Red box + Box ' + (i + 1) + ' = ' + overlaps(box, this) + '</p>';
    }).get().join('');

    //$('body').append( html );
});


var Wave = (function() {
    'use strict';

    function colorHex(color) {
        var that = color;
        //åå…­è¿›åˆ¶é¢œè‰²å€¼çš„æ­£åˆ™è¡¨è¾¾å¼
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        // å¦‚æžœæ˜¯rgbé¢œè‰²è¡¨ç¤º
        if (/^(rgb|RGB)/.test(that)) {
            var aColor = that.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
            var strHex = "#";
            for (var i = 0; i < aColor.length; i++) {
                var hex = Number(aColor[i]).toString(16);
                if (hex.length < 2) {
                    hex = '0' + hex;
                }
                strHex += hex;
            }
            if (strHex.length !== 7) {
                strHex = that;
            }
            return strHex;
        } else if (reg.test(that)) {
            var aNum = that.replace(/#/, "").split("");
            if (aNum.length === 6) {
                return that;
            } else if (aNum.length === 3) {
                var numHex = "#";
                for (var i = 0; i < aNum.length; i += 1) {
                    numHex += (aNum[i] + aNum[i]);
                }
                return numHex;
            }
        }
        return that;
    }

    function colorRgb(color, opacity) {
        var sColor = color.toLowerCase();
        //åå…­è¿›åˆ¶é¢œè‰²å€¼çš„æ­£åˆ™è¡¨è¾¾å¼
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        // å¦‚æžœæ˜¯16è¿›åˆ¶é¢œè‰²
        if (sColor && reg.test(sColor)) {
            if (sColor.length === 4) {
                var sColorNew = "#";
                for (var i = 1; i < 4; i += 1) {
                    sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
                }
                sColor = sColorNew;
            }
            //å¤„ç†å…­ä½çš„é¢œè‰²å€¼
            var sColorChange = [];
            for (var i = 1; i < 7; i += 2) {
                sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
            }
            return "rgba(" + sColorChange.join(",") + "," + opacity + ")";
        }
        return sColor;
    }

    var Wave = /** @class */ (function() {
        function Wave(container, options) {
            var originOption = {
                number: 3,
                smooth: 50,
                velocity: 1,
                height: .3,
                colors: ['#ff7657'],
                border: {
                    show: false,
                    width: 2,
                    color: ['']
                },
                opacity: .5,
                position: 'bottom',
            };
            this.container = document.querySelector(container);
            this.options = Object.assign(originOption, options);
            this.lines = [];
            this.frame = null;
            this.step = 0;
            this.status = 'pause';
            this.init();
            this.draw();
        }
        Wave.prototype.init = function() {
            if (this.container.querySelector('canvas') === null) {
                var canvas = document.createElement('canvas');
                this.container.appendChild(canvas);
            }
            this.canvas = this.container.querySelector('canvas');
            this.canvas.width = this.container.offsetWidth;
            this.canvas.height = this.container.offsetHeight;
            this.ctx = this.canvas.getContext('2d');
            this.setLines();
        };
        Wave.prototype.animate = function() {
            this.status = 'animating';
            this.draw();
        };
        Wave.prototype.pause = function() {
            cancelAnimationFrame(this.frame);
            this.frame = null;
            this.status = 'pause';
        };
        Wave.prototype.setOptions = function(options) {
            this.options = Object.assign(this.options, options);
            this.setLines();
            this.reset();
            if (this.status === 'pause') {
                this.draw();
            }
        };
        Wave.prototype.reset = function() {
            this.init();
        };
        Wave.prototype.draw = function() {
            var _this = this;
            var canvas = this.canvas;
            var ctx = this.ctx;
            var height = this.getWaveHeight();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            this.step += this.options.velocity;
            this.lines.forEach(function(line, index) {
                var angle = (_this.step + index * 180 / _this.lines.length) * Math.PI / 180;
                var leftHeight = Math.sin(angle) * _this.options.smooth;
                var rightHeight = Math.cos(angle) * _this.options.smooth;
                var vertexs = _this.getVertexs(leftHeight, rightHeight);
                ctx.fillStyle = line.rgba;
                ctx.beginPath();
                ctx.moveTo(vertexs[0][0], vertexs[0][1]);
                if (_this.options.border.show) {
                    ctx.lineWidth = _this.options.border.width;
                    ctx.strokeStyle = _this.options.border.color[index] ? _this.options.border.color[index] : line.hex;
                }
                if (_this.options.position === 'left' || _this.options.position === 'right') {
                    ctx.bezierCurveTo(height + leftHeight - _this.options.smooth, canvas.height / 2, height + rightHeight - _this.options.smooth, canvas.width / 2, vertexs[1][0], vertexs[1][1]);
                } else {
                    ctx.bezierCurveTo(canvas.width / 2, height + leftHeight - _this.options.smooth, canvas.width / 2, height + rightHeight - _this.options.smooth, vertexs[1][0], vertexs[1][1]);
                }
                if (_this.options.border.show) {
                    ctx.stroke();
                }
                ctx.lineTo(vertexs[2][0], vertexs[2][1]);
                ctx.lineTo(vertexs[3][0], vertexs[3][1]);
                ctx.lineTo(vertexs[0][0], vertexs[0][1]);
                ctx.closePath();
                ctx.fill();
            });
            var that = this;
            if (this.status === 'animating') {
                this.frame = requestAnimationFrame(function() {
                    that.draw();
                });
            }
        };
        Wave.prototype.setLines = function() {
            this.lines = [];
            for (var i = 0; i < this.options.number; i++) {
                var color = this.options.colors[i % this.options.colors.length];
                var line = {
                    hex: colorHex(color),
                    rgba: colorRgb(color, this.options.opacity)
                };
                this.lines.push(line);
            }
        };
        Wave.prototype.getVertexs = function(leftHeight, rightHeight) {
            var canvasHeight = this.canvas.height;
            var canvasWidth = this.canvas.width;
            var waveHeight = this.getWaveHeight();
            switch (this.options.position) {
                case 'bottom':
                    return [
                        [0, waveHeight + leftHeight],
                        [canvasWidth, waveHeight + rightHeight],
                        [canvasWidth, canvasHeight],
                        [0, canvasHeight]
                    ];
                case 'top':
                    return [
                        [0, waveHeight + leftHeight],
                        [canvasWidth, waveHeight + rightHeight],
                        [canvasWidth, 0],
                        [0, 0],
                    ];
                case 'left':
                    return [
                        [waveHeight + leftHeight, 0],
                        [waveHeight + rightHeight, canvasHeight],
                        [0, canvasHeight],
                        [0, 0],
                    ];
                case 'right':
                    return [
                        [waveHeight + leftHeight, 0],
                        [waveHeight + rightHeight, canvasHeight],
                        [canvasWidth, canvasHeight],
                        [canvasWidth, 0],
                    ];
            }
        };
        Wave.prototype.getWaveHeight = function() {
            if (this.options.height > 1) {
                switch (this.options.position) {
                    case 'bottom':
                        return this.canvas.height - this.options.height;
                    case 'top':
                        return this.options.height;
                    case 'left':
                        return this.options.height;
                    case 'right':
                        return this.canvas.width - this.options.height;
                }
            } else {
                switch (this.options.position) {
                    case 'bottom':
                        return this.canvas.height * (1 - this.options.height);
                    case 'top':
                        return this.canvas.height * this.options.height;
                    case 'left':
                        return this.canvas.width * this.options.height;
                    case 'right':
                        return this.canvas.width * (1 - this.options.height);
                }
            }
        };
        return Wave;
    }());

    return Wave;

}());
const waveAnimation = new Wave("#contacto", {
    // number of waves
    number: 2,
    // smoothness
    smooth: 40,
    // animation speed
    velocity: 1,
    // height in pixels or percent
    height: .100,
    // color
    colors: ['rgb(230, 163, 27)', '#121212', ],
    // border options
    border: {
        show: false,
        width: 2,
        color: ['']
    },
    // opacity
    opacity: 1,
    // 'top' | 'bottom' | 'left' | 'right'
    position: 'bottom'

})
waveAnimation.animate();